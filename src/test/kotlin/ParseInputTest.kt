import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

const val simpleTestInput = "src/test/resources/input-1.txt"

internal class ParseInputTest {

    @Test
    internal fun `simple input returns single list entry`() {
        val readResult = read(simpleTestInput)
        val parseResult = parse(readResult[0])

        assertThat(readResult).hasSize(1)
        assertThat(parseResult.first.digitPatterns)
            .containsExactlyInAnyOrderElementsOf(
                listOf(
                    "acedgfb",
                    "cdfbe",
                    "gcdfa",
                    "fbcad",
                    "dab",
                    "cefabd",
                    "cdfgeb",
                    "eafb",
                    "cagedb",
                    "ab"
                )
            )

        assertThat(parseResult.second.digits)
            .containsExactlyInAnyOrderElementsOf(
                listOf(
                    "cdfeb",
                    "fcadb",
                    "cdfeb",
                    "cdbaf"
                )
            )
    }
}
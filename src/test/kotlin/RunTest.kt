import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

const val testInput = "src/test/resources/input-2.txt"
internal class RunTest {
    @Test
    internal fun `test part 1`() {
        assertThat(part1(testInput)).isEqualTo(26)
    }

    @Test
    internal fun `test part 2`() {
        assertThat(part2(testInput)).isEqualTo(61229)
    }
}
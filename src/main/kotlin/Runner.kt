
const val input = "src/main/resources/input.txt"
fun main() {
    println(part1(input))
    println(part2(input))
}
import java.io.File

enum class Digit(val digit: Char) {
    ONE('1'), TWO('2'), THREE('3'), FOUR('4'), FIVE('5'), SIX('6'), SEVEN('7'), EIGHT('8'), NINE('9'), ZERO('0')
}

data class Patterns(
    val digitPatterns: List<String>
) {
    fun digits(): Map<Digit, String> {
        return mapOf(
            Digit.ONE to onePattern(),
            Digit.TWO to twoPattern(),
            Digit.THREE to threePattern(),
            Digit.FOUR to fourPattern(),
            Digit.FIVE to fivePattern(),
            Digit.SIX to sixPattern(),
            Digit.SEVEN to sevenPattern(),
            Digit.EIGHT to eightPattern(),
            Digit.NINE to ninePattern(),
            Digit.ZERO to zeroPattern(),
        )
    }

    private fun onePattern(): String {
        return digitPatterns.single { it.length == 2 }
    }

    private fun twoPattern(): String {
        return digitPatterns.single {
            it.length == 5
                    && !contentEquals(it, fivePattern())
                    && !contentEquals(it, threePattern())
        }
    }

    private fun threePattern(): String {
        return digitPatterns.single {
            it.length == 5
                    && containsAll(it, sevenPattern())
        }
    }

    private fun fourPattern(): String {
        return digitPatterns.single { it.length == 4 }
    }

    private fun fivePattern(): String {
        return digitPatterns.single {
            it.length == 5
                    && containsAll(ninePattern(), it)
                    && !contentEquals(it, threePattern())
        }
    }

    private fun sixPattern(): String {
        return digitPatterns.single {
            it.length == 6
                    && !contentEquals(it, ninePattern())
                    && !contentEquals(it, zeroPattern())
        }
    }

    private fun sevenPattern(): String {
        return digitPatterns.single { it.length == 3 }
    }

    private fun eightPattern(): String {
        return digitPatterns.single { it.length == 7 }
    }

    private fun ninePattern(): String {
        return digitPatterns.single {
            it.length == 6 && containsAll(it, fourPattern())
        }
    }

    private fun zeroPattern(): String {
        return digitPatterns.single {
            it.length == 6
                    && containsAll(it, sevenPattern())
                    && !containsAll(it, fourPattern())
        }
    }

    private fun contentEquals(first: String, second: String): Boolean {
        return first.toSortedSet() == second.toSortedSet()
    }

    private fun containsAll(first: String, second: String): Boolean {
        return first.toSortedSet().containsAll(second.toSortedSet())
    }
}

data class Input(
    val digits: List<String>
) {
    fun parse(patterns: Patterns): String {
        val digitPatterns = patterns.digits()
        return digits.joinToString(separator = "") {
            val digits = digitPatterns.filter { e -> e.value.toSortedSet() == it.toSortedSet() }
                .map { e -> e.key }
                .map { d -> d.digit }
                .singleOrNull() ?: Digit.ZERO.digit
            return@joinToString digits.toString()
        }
    }
}

fun read(file: String): List<String> {
    return File(file).useLines { it.toList() }
}

fun parse(input: String): Pair<Patterns, Input> {
    val parts = input.split(" | ")
    return Patterns(parts[0].split(" ").toList()) to Input(parts[1].split(" ").toList())
}

fun part1(file: String): Int {
    return read(file)
        .map { parse(it) }
        .map { it.second.parse(it.first) }
        .sumOf {
            it.toCharArray().count { c ->
                listOf(1, 4, 7, 8).contains(c.digitToInt())
            }
        }
}

fun part2(file: String): Int {
    return read(file)
        .map { parse(it) }
        .map { it.second.parse(it.first) }
        .sumOf { it.toInt() }
}